import Foundation
import UIKit
import CoreMotion

class Manager {
    
    static let shared = Manager()
    private init() {}
    
    var score = 0
    var playTime: Double = 0
    var difficultyCounter = UserDefaults.standard.object(forKey: Keys.difficultyCounter.rawValue) as? Double ?? 1.0
    var easyDifficulty: Float = 0
    var mediumDifficulty: Float = 1
    var hardDifficulty: Float = 2
    var obstacleAppearanceInterval = 1.0
    var speedStep = 20
    var vehicleImagePath = ""
    private let defaults = UserDefaults.standard
    let singleScoreKey = "codable"
    let arrayScoreKey = "codable"
    
    var nitroCounter: Double = 12.0
    var nitroUsed: Double = 0.0
    var usedNitroTimer = Timer()
    var didTiltForward: Bool = false
    var lastShakenDate: Date = Date(timeIntervalSinceNow: -2)
    var tiltStartedDate: Date = Date(timeIntervalSinceNow: -2)
    var tiltForwardInterval: Double = 3.0
    var tiltBackInterval: Double = 0.5
    
    
    func blinkEffect(appliedView: UIView) {
        let blink = UIView()
        let blinkOrigin = CGPoint(x: 0, y: 0)
        blink.frame = CGRect(origin: blinkOrigin, size: appliedView.frame.size)
        blink.backgroundColor = .white
        blink.alpha = 0.65
        appliedView.addSubview(blink)
        UIView.animate(withDuration: 0.3, animations: {
            blink.alpha = 0.0
        }) { (_) in
            blink.removeFromSuperview()
        }
    }
    
    func shakeDeviceUsingGyro(rotation: CMRotationRate, didShake: @escaping (Bool) -> ()) {
        if nitroCounter == 0.0 {
            if fabs(rotation.x) > tiltForwardInterval && fabs(lastShakenDate.timeIntervalSinceNow) > 0.3 {
                if !didTiltForward == true {
                    tiltStartedDate = Date()
                    if rotation.x > 0 {
                        didTiltForward = true
                    } else {
                        didTiltForward = false
                    }
                }
            }
            
            if fabs(tiltStartedDate.timeIntervalSinceNow) >= 0.3 {
                didTiltForward = false
            } else {
                if fabs(rotation.x) > tiltBackInterval {
                    if didTiltForward == true && rotation.x > 0 {
                        lastShakenDate = Date()
                        didTiltForward = false
                        nitroCounter = 12.0
                        nitroUsed = 4.0
                        didShake(true)
                        print("Shaken")
                    }
                }
            }
        }
    }
    
    func nitroCountdown() {
        nitroCounter -= 0.1
        if nitroCounter <= 0.0 {
            nitroCounter = 0.0
        }
    }
    
    func nitroLaunched() {
        usedNitroTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(nitroTimerRepeats), userInfo: nil, repeats: false)
    }
    
    @objc func nitroTimerRepeats() {
        print(nitroUsed)
        nitroUsed -= 0.1
        if nitroUsed > 0.0 {
            self.nitroLaunched()
        } else {
            nitroUsed = 0.0
            speedStep -= 10
            print(nitroUsed)
        }
        
    }
    
    func scoreCountStart() {
        score += Int(difficultyCounter)
        playTime += 0.1
    }
    
    func scoreCountStop() {
        score = 0
        playTime = 0
    }
    
    func save(scores: Scores) {
        defaults.set(encodable: scores, forKey: singleScoreKey)
    }
    
    func load() -> Scores? {
        let scores = defaults.value(Scores.self, forKey: singleScoreKey)
        return scores
    }
    
    func saveScoresArray(item: Scores) {
        var scoresArray = Manager.shared.loadScoresArray()
        scoresArray.append(item)
        defaults.set(encodable: scoresArray, forKey: arrayScoreKey)
    }
    
    func loadScoresArray() -> [Scores] {
        let scores = defaults.value([Scores].self, forKey: arrayScoreKey)
        return scores ?? []
    }
    
    
    func saveImage(image: UIImage) -> String? {
        
        //1. Получить директорию
        guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil}
        
        //2. создать уникальное имя файла
        let fileName = UUID().uuidString
        
        //3. Создать путь: директория + имя файла
        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        
        //4. Преобразовать UIImage в 0101110100111
        guard let data = image.pngData() else { return nil}
        
        //5. Проверяем, есть ли там уже файл, если да - удаляем
        if FileManager.default.fileExists(atPath: fileURL.path) {
            do {
                try FileManager.default.removeItem(atPath: fileURL.path)
                print("Removed old image")
            } catch let removeError {
                print("Couldn't remove the file at path", removeError)
            }
            
        }
        
        //6. Записываем п.4 по пути п.3
        do {
            try data.write(to: fileURL)
            return fileName
        } catch let error {
            print("Error saving the file with error", error)
            return nil
        }
        
    }
    
    func loadImage(fileName:String) -> UIImage? {
        let documentDirectory = FileManager.SearchPathDirectory.documentDirectory
        
        let userDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(documentDirectory, userDomainMask, true)
        
        if let dirPath = paths.first {
            let imageUrl = URL(fileURLWithPath: dirPath).appendingPathComponent(fileName)
            let image = UIImage(contentsOfFile: imageUrl.path)
            return image
            
        }
        return nil
    }
    
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension UIScrollView {
    var currentPage: Int {
        return Int((self.contentOffset.x + (0.5 * self.frame.size.width)) / self.frame.width) + 1
    }
}

extension UserDefaults {
    
    func set<T: Encodable>(encodable: T, forKey key: String) {
        if let data = try? JSONEncoder().encode(encodable) {
            set(data, forKey: key)
        }
    }
    
    func value<T: Decodable>(_ type: T.Type, forKey key: String) -> T? {
        if let data = object(forKey: key) as? Data,
            let value = try? JSONDecoder().decode(type, from: data) {
            return value
        }
        return nil
    }
}
