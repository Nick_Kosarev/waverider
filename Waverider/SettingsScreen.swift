import UIKit

//MARK: - Enums
enum Keys: String {
    case difficultySliderValue = "difficultySliderValue"
    case difficultyLabelText = "difficultyLabelText"
    case nameTextFieldText = "nameTextFieldText"
    case speedStep = "speedStep"
    case obstacleAppearanceInterval = "obstacleAppearanceInterval"
    case difficultyCounter = "difficultyCounter"
}


class SettingsScreen: UIViewController, UITextFieldDelegate, UIScrollViewDelegate {
    
//MARK: -  Constants and variables
    @IBOutlet weak var difficultySlider: UISlider!
    @IBOutlet weak var difficultyLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var vehicleSelector: UIScrollView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var middleView: UIView!
    @IBOutlet weak var bottomView: UIView!
    var secondVehicle = UIImage(named: "vehicle2")
    var thirdVehicle = UIImage(named: "vehicle3")
    let scrollViewNumberOfPages: Int = 1
    var vehicleImageListIndex: Int = 0
    
//MARK: - Lifecycle functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.applyGradient(colours: [.systemTeal, .violetColor, .indigoColor], locations: [0.0, 0.4, 0.7])
        self.view.addBlurEffect()
        
        self.vehicleSelector.layer.cornerRadius = 20
        self.assignImage()
        self.hideKeyboardWhenTappedAround()
        self.nameTextField.delegate = self
        self.vehicleSelector.delegate = self
        
        view.bringSubviewToFront(topView)
        view.bringSubviewToFront(middleView)
        view.bringSubviewToFront(bottomView)
        view.bringSubviewToFront(backButton)

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let nameTextFieldText = UserDefaults.standard.object(forKey: Keys.nameTextFieldText.rawValue) else { return }
        nameTextField.text = nameTextFieldText as? String
        difficultySlider.setValue(UserDefaults.standard.float(forKey: Keys.difficultySliderValue.rawValue), animated: true)
        guard let difficultyLabelText = UserDefaults.standard.object(forKey: Keys.difficultyLabelText.rawValue) else { return }
        difficultyLabel.text = difficultyLabelText as? String
        guard let speedStep = UserDefaults.standard.object(forKey: Keys.speedStep.rawValue) else { return }
        Manager.shared.speedStep = speedStep as? Int ?? 20
        guard let obstacleAppearanceInterval = UserDefaults.standard.object(forKey: Keys.obstacleAppearanceInterval.rawValue) else { return }
        Manager.shared.obstacleAppearanceInterval = obstacleAppearanceInterval as? Double ?? 1.0
        guard let difficultyCounter = UserDefaults.standard.object(forKey: Keys.difficultyCounter.rawValue) else { return }
        Manager.shared.difficultyCounter = difficultyCounter as? Double ?? 1.0
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UserDefaults.standard.set(nameTextField.text, forKey: Keys.nameTextFieldText.rawValue)
        UserDefaults.standard.set(difficultySlider.value, forKey: Keys.difficultySliderValue.rawValue)
        UserDefaults.standard.set(difficultyLabel.text, forKey: Keys.difficultyLabelText.rawValue)
        UserDefaults.standard.set(Manager.shared.speedStep, forKey: Keys.speedStep.rawValue)
        UserDefaults.standard.set(Manager.shared.obstacleAppearanceInterval, forKey: Keys.obstacleAppearanceInterval.rawValue)
        UserDefaults.standard.set(Manager.shared.difficultyCounter, forKey: Keys.difficultyCounter.rawValue)
    }
    
//MARK: - IBActions
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func difficultySliderChanged(_ sender: UISlider) {
        let step: Float = 1
        let roundedValue = round(sender.value / step) * step
        sender.value = roundedValue
        switch sender.value {
        case Manager.shared.easyDifficulty:
            difficultyLabel.text = "Easy"
            Manager.shared.speedStep = 25
            Manager.shared.obstacleAppearanceInterval = 1.0
            Manager.shared.difficultyCounter = 1.0
        case Manager.shared.mediumDifficulty:
            difficultyLabel.text = "Medium"
            Manager.shared.speedStep = 30
            Manager.shared.obstacleAppearanceInterval = 0.7
            Manager.shared.difficultyCounter = 1.5
        case Manager.shared.hardDifficulty:
            difficultyLabel.text = "Hard"
            Manager.shared.speedStep = 35
            Manager.shared.obstacleAppearanceInterval = 0.5
            Manager.shared.difficultyCounter = 2.0
        default:
            break
        }
    }
    
//MARK: - Flow functions
    
    private func assignImage() {
        let padding : CGFloat = 15
        let vehicleImageViewWidth = vehicleSelector.frame.size.width - 2 * padding
        let vehicleImageViewHeight = vehicleSelector.frame.size.height - 2 * padding
        vehicleSelector.autoresizingMask = [UIScrollView.AutoresizingMask.flexibleLeftMargin, UIScrollView.AutoresizingMask.flexibleRightMargin, UIScrollView.AutoresizingMask.flexibleTopMargin, UIScrollView.AutoresizingMask.flexibleBottomMargin]
        
        let vehicleImageList = [thirdVehicle, secondVehicle]
        var vehicleImageOriginX : CGFloat = 0
        for _ in 0...scrollViewNumberOfPages {
            let vehicleImageView = UIImageView()
            vehicleImageView.frame = CGRect(x: vehicleImageOriginX + padding, y: padding, width: vehicleImageViewWidth, height: vehicleImageViewHeight)
            vehicleSelector.addSubview(vehicleImageView)
            vehicleImageOriginX = vehicleImageView.frame.origin.x + vehicleImageViewWidth + padding
            vehicleImageView.image = vehicleImageList[vehicleImageListIndex]
            vehicleImageView.contentMode = .scaleAspectFit
            vehicleImageListIndex += 1
        }
        
        
        vehicleSelector.contentSize = CGSize(width: vehicleImageOriginX + padding, height: vehicleSelector.frame.size.height)
        
        
    }
    
//MARK: - Delegate methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let currentPage = scrollView.currentPage
        vehicleImageListIndex = scrollView.currentPage - 1
        let vehicleImageList = [thirdVehicle, secondVehicle]
        let imagePathSave = Manager.shared.saveImage(image: vehicleImageList[vehicleImageListIndex]!)
        Manager.shared.vehicleImagePath = imagePathSave!
    }
    
}


