import Foundation

class Scores: NSObject, Codable {
    var uuid: String
    var name: String?
    var totalScore: String?
    var totalTime: String?
    var finishDate: Date?
    
    public enum CodingKeys: String, CodingKey {
        case uuid, name, totalScore, totalTime, finishDate
    }
    
    public override init() {
        uuid = UUID().uuidString // unique ID
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.name = try container.decodeIfPresent(String.self, forKey: .name)
        self.totalScore = try container.decodeIfPresent(String.self, forKey: .totalScore)
        self.totalTime = try container.decodeIfPresent(String.self, forKey: .totalTime)
        self.finishDate = try container.decodeIfPresent(Date.self, forKey: .finishDate)
        self.uuid = try container.decodeIfPresent(String.self, forKey: .uuid) ?? String()
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(self.name, forKey: .name)
        try container.encode(self.totalScore, forKey: .totalScore)
        try container.encode(self.totalTime, forKey: .totalTime)
        try container.encode(self.finishDate, forKey: .finishDate)
        try container.encode(self.uuid, forKey: .uuid)
    }
}

