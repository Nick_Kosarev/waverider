import UIKit

class CustomTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func configureCell(with object: Scores) {
        nameLabel.text = object.name
        scoreLabel.text = object.totalScore
        guard let number = Int(object.totalTime ?? String()) else { return }
        let timeformatter = DateComponentsFormatter()
        timeformatter.allowedUnits = [.hour, .minute, .second]
        timeformatter.unitsStyle = .abbreviated
        guard let timestring = timeformatter.string(from: TimeInterval(number)) else { return }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM.dd"
        let datestring = formatter.string(from: object.finishDate ?? Date())
        timeLabel.text = timestring
        dateLabel.text = datestring
    }

}
