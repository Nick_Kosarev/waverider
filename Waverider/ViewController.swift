import UIKit
import CoreMotion

//MARK: - Enums
enum Direction {
    case left
    case right
}

class ViewController: UIViewController {
    
//MARK: -  IBOutlets
    @IBOutlet weak var layout: UIView!
    @IBOutlet weak var bottomBar: UIView!
    @IBOutlet weak var startButton: UILabel!
    @IBOutlet weak var road: UIView!
    @IBOutlet weak var leftLane: UIView!
    @IBOutlet weak var rightLane: UIView!
    @IBOutlet weak var leftRoadside: UIView!
    @IBOutlet weak var rightRoadside: UIView!
    @IBOutlet weak var firstRoadmark: UIView!
    @IBOutlet weak var secondRoadmark: UIView!
    @IBOutlet weak var thirdRoadmark: UIView!
    @IBOutlet weak var firstTree: UIImageView!
    @IBOutlet weak var secondTree: UIImageView!
    @IBOutlet weak var thirdTree: UIImageView!
    @IBOutlet weak var fourthTree: UIImageView!
    @IBOutlet weak var fifthTree: UIImageView!
    @IBOutlet weak var sixthTree: UIImageView!
    @IBOutlet weak var extraTreeLeft: UIImageView!
    @IBOutlet weak var extraTreeRight: UIImageView!
    @IBOutlet weak var vehicle: UIImageView!
    @IBOutlet weak var leftButton: UILabel!
    @IBOutlet weak var rightButton: UILabel!
    @IBOutlet weak var vehicleCenter: NSLayoutConstraint!
    @IBOutlet weak var leftEdge: UIView!
    @IBOutlet weak var rightEdge: UIView!
    @IBOutlet weak var nitroLabel: UILabel!
    
//MARK: - Constants and variables
    var motionManager = CMMotionManager()
    var rotationX: Double = 0
    var accelerometerStarted: Bool = false
    
    var timeInterval: Double = 0.1
    var speedStep: CGFloat = CGFloat(Manager.shared.speedStep)
    var vehicleStepMultiplier: CGFloat = 0.03
    var roadMarkDistance: CGFloat = 40
    var treesConstraint: CGFloat = 100
    var animationTime: Double = 0.2
    weak var vehicleMovementTimer: Timer?
    var obstacleMovementTimerLeft = Timer()
    var obstacleMovementTimerRight = Timer()
    var randomObstacleTimer = Timer()
    var playTimer = Timer()
    var obstacleAppearanceRandomness = 8
    var obstacleAppearanceInterval = Manager.shared.obstacleAppearanceInterval
    var isPlaying: Bool = false
    var allTimers: [Timer] = []
    var roadMarkArray: [UIView] = []
    var treesArrayLeft: [UIView] = []
    var treesArrayRight: [UIView] = []
    
//MARK: - Lifecycle functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.roadMarkArray = [self.firstRoadmark, self.secondRoadmark, self.thirdRoadmark]
        self.treesArrayLeft = [self.firstTree, self.secondTree, self.thirdTree, self.extraTreeLeft]
        self.treesArrayRight = [self.fourthTree, self.fifthTree, self.sixthTree, self.extraTreeRight]
        self.leftEdge.backgroundColor = .lightPurple
        self.leftEdge.applyShadow(color: .neonPurple, opacity: 1.0, offset: .zero, radius: 6, viewCornerRadius: nil)
        self.rightEdge.backgroundColor = .lightPurple
        self.rightEdge.applyShadow(color: .neonPurple, opacity: 1.0, offset: .zero, radius: 6, viewCornerRadius: nil)
        self.firstRoadmark.applyShadow(color: .systemTeal, opacity: 0.8, offset: .zero, radius: 6, viewCornerRadius: nil)
        self.secondRoadmark.applyShadow(color: .systemTeal, opacity: 0.8, offset: .zero, radius: 6, viewCornerRadius: nil)
        self.thirdRoadmark.applyShadow(color: .systemTeal, opacity: 0.8, offset: .zero, radius: 6, viewCornerRadius: nil)
        self.leftButton.applyShadow(color: .white, opacity: 0.7, offset: .zero, radius: 5, viewCornerRadius: nil)
        self.rightButton.applyShadow(color: .white, opacity: 0.7, offset: .zero, radius: 5, viewCornerRadius: nil)
        self.startButton.applyShadow(color: .white, opacity: 0.7, offset: .zero, radius: 5, viewCornerRadius: nil)
        
        let startButtonTouch = UITapGestureRecognizer(target: self, action: #selector(startButtonTouched(_:)))
        self.startButton.addGestureRecognizer(startButtonTouch)
        
        let leftButtonHold = UILongPressGestureRecognizer(target: self, action: #selector(leftButtonHeld(_:)))
        leftButtonHold.minimumPressDuration = 0
        self.leftButton.addGestureRecognizer(leftButtonHold)
        
        let rightButtonHold = UILongPressGestureRecognizer(target: self, action: #selector(rightButtonHeld(_:)))
        rightButtonHold.minimumPressDuration = 0
        self.rightButton.addGestureRecognizer(rightButtonHold)
        
        let panVehicle = UIPanGestureRecognizer(target: self, action: #selector(panVehicle(_:)))
        self.road.addGestureRecognizer(panVehicle)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let vehicleStep = UserDefaults.standard.object(forKey: Keys.speedStep.rawValue) else { return }
        Manager.shared.speedStep = vehicleStep as? Int ?? 20
        guard let obstacleInterval = UserDefaults.standard.object(forKey: Keys.obstacleAppearanceInterval.rawValue) else { return }
        Manager.shared.obstacleAppearanceInterval = obstacleInterval as? Double ?? 1.0
        speedStep = CGFloat(Manager.shared.speedStep)
        obstacleAppearanceInterval = Manager.shared.obstacleAppearanceInterval
        
        if let vehicleImage = Manager.shared.loadImage(fileName: Manager.shared.vehicleImagePath) {
            self.vehicle.image = vehicleImage
        }
        self.vehicle.applyShadow(color: self.vehicle.image?.averageColor, opacity: 1.0, offset: .zero, radius: 7, viewCornerRadius: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.leftLane.applyGradient(colours: [.violetColor, .indigoColor], locations: [0.0, 0.3])
        self.rightLane.applyGradient(colours: [.violetColor, .indigoColor], locations: [0.0, 0.3])
        self.bottomBar.applyGradientBottomView(colours: [.indigoColor, .violetColor, .systemTeal], locations: [0.0, 0.7, 0.9])
        self.bottomBar.addBlurEffect()
        self.bottomBar.bringSubviewToFront(leftButton)
        self.bottomBar.bringSubviewToFront(rightButton)
        self.bottomBar.bringSubviewToFront(startButton)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.stopGame()
    }
    
    //MARK: - IBActions
    @IBAction private func startButtonTouched (_ sender: UITapGestureRecognizer) {
        if isPlaying {
            isPlaying = false
            self.stopGame()
            self.showScorePopup()
            self.startButton.text = "START"
        } else {
            isPlaying = true
            playTimer = Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: true) { (_) in
                self.roadmarkMovement()
                self.treesMovement()
                Manager.shared.scoreCountStart()
                Manager.shared.nitroCountdown()
                if Manager.shared.nitroCounter == 0.0 {
                    self.nitroLabel.isHidden = false
                }
                if Manager.shared.nitroUsed == 0.0 {
                    self.speedStep = CGFloat(Manager.shared.speedStep)
                }
            }
            Manager.shared.nitroCounter = 12.0
            self.startButton.text = "STOP"
            playTimer.fire()
            self.startObstacleTimer()
            self.gyroAndAccelerometerSettings()
        }
    }
    
    
    @IBAction private func leftButtonHeld (_ sender: UILongPressGestureRecognizer) {
        switch sender.state {
        case .began:
            UIView.animate(withDuration: animationTime) {
                self.vehicle.transform = CGAffineTransform(rotationAngle: -0.2)
            }
        case .ended:
            UIView.animate(withDuration: animationTime) {
                self.vehicle.transform = CGAffineTransform(rotationAngle: 0)
            }
        default:
            break
        }
        vehicleMovementTimer?.invalidate()
        vehicleMovementTimer = Timer.scheduledTimer(withTimeInterval: 0.05, repeats: true) { (timer) in
            switch sender.state {
            case .began:
                self.vehicleMovement(moveDirection: .left)
            case.ended:
                timer.invalidate()
            default:
                break
            }
        }
        vehicleMovementTimer?.fire()
    }
    
    @IBAction private func rightButtonHeld (_ sender: UILongPressGestureRecognizer) {
        switch sender.state {
        case .began:
            UIView.animate(withDuration: animationTime) {
                self.vehicle.transform = CGAffineTransform(rotationAngle: 0.2)
            }
        case .ended:
            UIView.animate(withDuration: animationTime) {
                self.vehicle.transform = CGAffineTransform(rotationAngle: 0)
            }
        default:
            break
        }
        vehicleMovementTimer?.invalidate()
        vehicleMovementTimer = Timer.scheduledTimer(withTimeInterval: 0.05, repeats: true) { (timer) in
            switch sender.state {
            case .began:
                self.vehicleMovement(moveDirection: .right)
            case.ended:
                timer.invalidate()
            default:
                break
            }
        }
        vehicleMovementTimer?.fire()
    }
    
    @IBAction private func panVehicle (_ sender: UIPanGestureRecognizer) {
        let velocity = sender.velocity(in: road)
        if velocity.x > 0 {
            UIView.animate(withDuration: animationTime) {
                self.vehicle.transform = CGAffineTransform(rotationAngle: 0.2)
            }
        } else if velocity.x < 0 {
            UIView.animate(withDuration: animationTime) {
                self.vehicle.transform = CGAffineTransform(rotationAngle: -0.2)
            }
        }
        switch sender.state {
        case .changed:
            if self.road.frame.contains(sender.location(in: self.view)) {
                let location = sender.location(in: self.road)
                self.vehicleCenter.constant = location.x - (road.frame.size.width - vehicle.frame.size.width)
            }
        case .ended:
            UIView.animate(withDuration: animationTime) {
                self.vehicle.transform = CGAffineTransform(rotationAngle: 0)
            }
        default:
            break
        }
        
        if self.vehicleMovementCheck(directionCheck: .left) == false || self.vehicleMovementCheck(directionCheck: .right) == false {
            sender.state = .ended
        }
    }
    
    //MARK: - Flow functions
    
    private func stopGame() {
        motionManager.stopGyroUpdates()
        motionManager.stopAccelerometerUpdates()
        vehicleMovementTimer?.invalidate()
        playTimer.invalidate()
        randomObstacleTimer.invalidate()
        for timer in allTimers {
            timer.invalidate()
        }
        layout.layer.removeAllAnimations()
        self.dismiss(animated: true, completion: nil)
    }
    
    private func showScorePopup() {
        if let scoreWindow = storyboard?.instantiateViewController(withIdentifier: "ScorePopupWindow") as? ScorePopupWindow {
            self.addChild(scoreWindow)
            scoreWindow.view.frame = self.view.frame
            self.view.addSubview(scoreWindow.view)
            scoreWindow.didMove(toParent: self)
        }
    }
    
    private func startObstacleTimer() {
        randomObstacleTimer = Timer.scheduledTimer(withTimeInterval: obstacleAppearanceInterval, repeats: true) { (_) in
            self.createObstacleLeftLane()
            self.createObstacleRightLane()
        }
    }
    
    private func createObstacleLeftLane() {
        let randomNumber = Int(arc4random_uniform(UInt32(obstacleAppearanceRandomness)) + 1)
        if randomNumber == obstacleAppearanceRandomness {
            let randomWidth = CGFloat(arc4random_uniform(UInt32(leftLane.frame.size.width - vehicle.frame.size.width - 20)) + 20)
            let randomHeight = CGFloat(arc4random_uniform(UInt32(vehicle.frame.size.height - 20)) + 20)
            let randomPosition = CGFloat(arc4random_uniform(UInt32(leftLane.frame.size.width - randomWidth)))
            let obstacle = UIView()
            obstacle.frame = CGRect(x: randomPosition, y: leftLane.frame.origin.y - randomHeight, width: randomWidth , height: randomHeight)
            obstacle.backgroundColor = .clear
            obstacle.applyShadow(color: .random, opacity: 1.0, offset: .zero, radius: 5, viewCornerRadius: nil)
            obstacle.layer.borderColor = obstacle.layer.shadowColor
            obstacle.layer.borderWidth = 4.0
            road.addSubview(obstacle)
            obstacleMovementTimerLeft = Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: true) { (_) in
                UIView.animate(withDuration: self.animationTime) {
                    obstacle.frame.origin.y += self.speedStep
                }
                if obstacle.frame.origin.y >= self.leftLane.frame.size.height {
                    obstacle.removeFromSuperview()
                }
                if Manager.shared.nitroUsed == 0.0 {
                    if self.vehicle.frame.intersects(obstacle.frame) {
                        self.stopGame()
                        self.showScorePopup()
                    }
                } else {
                    if self.vehicle.frame.intersects(obstacle.frame) {
                        self.obstacleMovementTimerLeft.invalidate()
                        obstacle.removeFromSuperview()
                        Manager.shared.blinkEffect(appliedView: self.layout)
                    }
                }
            }
            obstacleMovementTimerLeft.fire()
            allTimers.append(obstacleMovementTimerLeft)
        }
        randomObstacleTimer.invalidate()
        self.startObstacleTimer()
    }
    
    private func createObstacleRightLane() {
        let randomNumber = Int(arc4random_uniform(UInt32(obstacleAppearanceRandomness)) + 1)
        if randomNumber == obstacleAppearanceRandomness {
            let randomWidth = CGFloat(arc4random_uniform(UInt32(rightLane.frame.size.width - vehicle.frame.size.width - 20)) + 20)
            let randomHeight = CGFloat(arc4random_uniform(UInt32(vehicle.frame.size.height - 20)) + 20)
            let randomPosition = CGFloat(arc4random_uniform(UInt32(rightLane.frame.size.width - randomWidth)) + UInt32(leftLane.frame.size.width))
            let obstacle = UIView()
            obstacle.frame = CGRect(x: randomPosition, y: rightLane.frame.origin.y - randomHeight, width: randomWidth , height: randomHeight)
            obstacle.backgroundColor = .clear
            obstacle.applyShadow(color: .random, opacity: 1.0, offset: .zero, radius: 5, viewCornerRadius: nil)
            obstacle.layer.borderColor = obstacle.layer.shadowColor
            obstacle.layer.borderWidth = 4.0
            road.addSubview(obstacle)
            obstacleMovementTimerRight = Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: true) { (_) in
                UIView.animate(withDuration: self.animationTime) {
                    obstacle.frame.origin.y += self.speedStep
                }
                if obstacle.frame.origin.y >= self.rightLane.frame.size.height {
                    obstacle.removeFromSuperview()
                }
                if Manager.shared.nitroUsed == 0.0 {
                    if self.vehicle.frame.intersects(obstacle.frame) {
                        self.stopGame()
                        self.showScorePopup()
                    }
                } else {
                    if self.vehicle.frame.intersects(obstacle.frame) {
                        self.obstacleMovementTimerRight.invalidate()
                        obstacle.removeFromSuperview()
                        Manager.shared.blinkEffect(appliedView: self.layout)
                    }
                }
            }
            obstacleMovementTimerRight.fire()
            allTimers.append(obstacleMovementTimerRight)
        }
        randomObstacleTimer.invalidate()
        self.startObstacleTimer()
    }
    
    
    private func vehicleMovement(moveDirection: Direction) {
        let vehicleStep: CGFloat = (road.frame.size.width * vehicleStepMultiplier) + CGFloat(rotationX)
        switch moveDirection {
        case .left:
            if self.vehicleMovementCheck(directionCheck: .left) {
                self.vehicleCenter.constant -= vehicleStep
                UIView.animate(withDuration: animationTime) {
                    self.road.layoutIfNeeded()
                }
            } else {
                vehicle.frame.origin.x = layout.frame.origin.x
            }
        case .right:
            if self.vehicleMovementCheck(directionCheck: .right) {
                self.vehicleCenter.constant += vehicleStep
                UIView.animate(withDuration: animationTime) {
                    self.road.layoutIfNeeded()
                }
            } else {
                vehicle.frame.origin.x = road.frame.size.width - vehicle.frame.size.width
            }
        }
    }
    
    
    private func vehicleMovementCheck(directionCheck: Direction) -> Bool {
        switch directionCheck {
        case .left:
            if vehicle.frame.origin.x <= layout.frame.origin.x + (road.frame.size.width * vehicleStepMultiplier) - 10 {
                self.stopGame()
                self.showScorePopup()
                return false
            } else {
                return true
            }
        case .right:
            if vehicle.frame.origin.x + vehicle.frame.size.width >= road.frame.size.width - (road.frame.size.width * vehicleStepMultiplier) + 10 {
                self.stopGame()
                self.showScorePopup()
                return false
            } else {
                return true }
        }
    }
    
    private func nextElementIndex(for viewArray: [UIView], viewIndex: Int) -> Int {
        var nextElementIndex: Int {
            var elementIndex = viewIndex + 1
            if elementIndex > viewArray.count - 1 {
                elementIndex = 0
            }
            if elementIndex < 0 {
                elementIndex = viewArray.count - 1
            }
            return elementIndex
        }
        return nextElementIndex
    }
    
    private func roadmarkMovement() {
        UIView.animate(withDuration: animationTime) {
            for element in self.roadMarkArray {
                element.frame.origin.y += self.speedStep
            }
        }
        
        for (index, element) in self.roadMarkArray.enumerated() {
            if element.frame.origin.y >= self.road.frame.size.height {
                let nextElement = self.roadMarkArray[self.nextElementIndex(for: self.roadMarkArray, viewIndex: index)]
                element.frame.origin.y = nextElement.frame.origin.y - element.frame.size.height - self.roadMarkDistance
            }
        }
    }
    
    private func treesMovement() {
        UIView.animate(withDuration: animationTime) {
            for element in self.treesArrayLeft {
                element.frame.origin.y += self.speedStep
            }
            for element in self.treesArrayRight {
                element.frame.origin.y += self.speedStep
            }
        }
        
        for (index, element) in self.treesArrayLeft.enumerated() {
            if element.frame.origin.y >= self.leftRoadside.frame.size.height {
                let nextElement = self.treesArrayLeft[self.nextElementIndex(for: self.treesArrayLeft, viewIndex: index)]
                element.frame.origin.y = nextElement.frame.origin.y - element.frame.size.height - self.treesConstraint
            }
        }
        
        for (index, element) in self.treesArrayRight.enumerated() {
            if element.frame.origin.y >= self.rightRoadside.frame.size.height {
                let nextElement = self.treesArrayRight[self.nextElementIndex(for: self.treesArrayRight, viewIndex: index)]
                element.frame.origin.y = nextElement.frame.origin.y - element.frame.size.height - self.treesConstraint
            }
        }
    }
    
//MARK: - Delegate methods
    private func gyroAndAccelerometerSettings() {
        if motionManager.isGyroAvailable {
            motionManager.deviceMotionUpdateInterval = 0.1
            motionManager.startDeviceMotionUpdates()
            motionManager.gyroUpdateInterval = 0.1
            motionManager.startGyroUpdates(to: .main) { (data, error) in
                Manager.shared.shakeDeviceUsingGyro(rotation: data!.rotationRate) { (didShake) in
                    if didShake == true {
                        self.nitroLabel.isHidden = true
                        Manager.shared.nitroLaunched()
                        Manager.shared.speedStep += 10
                        self.speedStep = CGFloat(Manager.shared.speedStep)
                    }
                }
            }
        } else {
            print("Gyro isn't recognized")
        }
        
        if motionManager.isAccelerometerAvailable {
            motionManager.accelerometerUpdateInterval = 0.1
            motionManager.startAccelerometerUpdates(to: .main) {
                [weak self] (data: CMAccelerometerData?, error: Error?) in
                
                switch data?.acceleration.x {
                case _ where data?.acceleration.x ?? 0 >= 0.2:
                    self?.accelerometerStarted = true
                    self?.rotationX = Double(Manager.shared.speedStep) * abs(data?.acceleration.x ?? 0)
                    self?.vehicleMovement(moveDirection: .right)
                    UIView.animate(withDuration: self?.animationTime ?? 0.3) {
                        self?.vehicle.transform = CGAffineTransform(rotationAngle: 0.2)
                    }
                case _ where data?.acceleration.x ?? 0 <= -0.2:
                    self?.accelerometerStarted = true
                    self?.rotationX = Double(Manager.shared.speedStep) * abs(data?.acceleration.x ?? 0)
                    self?.vehicleMovement(moveDirection: .left)
                    UIView.animate(withDuration: self?.animationTime ?? 0.3) {
                        self?.vehicle.transform = CGAffineTransform(rotationAngle: -0.2)
                    }
                case _ where data?.acceleration.x ?? 0 > -0.2 && data?.acceleration.x ?? 0 < 0.2:
                    self?.rotationX = 0
                    if self?.accelerometerStarted == true {
                        UIView.animate(withDuration: self?.animationTime ?? 0.3) {
                            self?.vehicle.transform = CGAffineTransform(rotationAngle: 0)
                            self?.accelerometerStarted = false
                        }
                    }
                default:
                    self?.rotationX = 0
                    self?.accelerometerStarted = false
                }
            }
        }
    }
    
}
