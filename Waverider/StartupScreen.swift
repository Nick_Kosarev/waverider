import UIKit

class StartupScreen: UIViewController {
    
//MARK: - IBOutlets
    @IBOutlet weak var StartButton: UIButton!
    @IBOutlet weak var SettingsButton: UIButton!
    @IBOutlet weak var ResultsButton: UIButton!
    @IBOutlet weak var layout: UIView!
    
//MARK: - Lifecycle functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.applyGradient(colours: [.systemTeal, .violetColor, .indigoColor], locations: [0.0, 0.4, 0.7])
        self.layout.addBlurEffect()
        StartButton.backgroundColor = .violetColor
        SettingsButton.backgroundColor = .violetColor
        ResultsButton.backgroundColor = .violetColor
        StartButton.layer.cornerRadius = 5
        StartButton.applyShadow(color: .violetColor, opacity: 0.7, offset: .zero, radius: 5, viewCornerRadius: 5)
        SettingsButton.layer.cornerRadius = 5
        SettingsButton.applyShadow(color: .violetColor, opacity: 0.7, offset: .zero, radius: 5, viewCornerRadius: 5)
        ResultsButton.layer.cornerRadius = 5
        ResultsButton.applyShadow(color: .violetColor, opacity: 0.7, offset: .zero, radius: 5, viewCornerRadius: 5)
        layout.bringSubviewToFront(StartButton)
        layout.bringSubviewToFront(SettingsButton)
        layout.bringSubviewToFront(ResultsButton)
        self.addParallaxToView(view: self.view, magnitude: 30)
    }
    
//MARK: - IBActions
    @IBAction func StartButtonPressed(_ sender: UIButton) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController {
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @IBAction func SettingsButtonPressed(_ sender: UIButton) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "SettingsScreen") as? SettingsScreen {
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @IBAction func ResultsButtonPressed(_ sender: UIButton) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ResultsScreen") as? ResultsScreen {
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
//MARK: - Flow functions
    func addParallaxToView(view: UIView, magnitude: Float) {
        let horizontal = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
        horizontal.minimumRelativeValue = -magnitude
        horizontal.maximumRelativeValue = magnitude
        
        let vertical = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
        vertical.minimumRelativeValue = -magnitude
        vertical.maximumRelativeValue = magnitude
        
        let group = UIMotionEffectGroup()
        group.motionEffects = [horizontal, vertical]
        view.addMotionEffect(group)
    }
    
    
    
}
