import UIKit

class ResultsScreen: UIViewController {
    
//MARK: - IBOutlets
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomView: UIView!
    
//MARK: - Constants and variables
    var totalScoreArray = Manager.shared.loadScoresArray()

//MARK: - Lifecycle functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.applyGradient(colours: [.systemTeal, .violetColor, .indigoColor], locations: [0.0, 0.4, 0.7])
        self.view.addBlurEffect()
        self.tableView.backgroundColor = .clear
        view.bringSubviewToFront(topView)
        view.bringSubviewToFront(tableView)
        view.bringSubviewToFront(bottomView)
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clearButtonPressed(_ sender: UIButton) {
        totalScoreArray.removeAll()
        tableView.reloadData()
        UserDefaults.standard.removeObject(forKey: Manager.shared.arrayScoreKey)
    }
}

//MARK: - Extensions

extension ResultsScreen: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        totalScoreArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? CustomTableViewCell else { return UITableViewCell()}
        cell.configureCell(with: totalScoreArray[indexPath.row])
        
        return cell
    }
    
}
