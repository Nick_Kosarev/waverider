import UIKit

class ScorePopupWindow: UIViewController {
    
//MARK: - IBOutlets
    @IBOutlet weak var popUpBox: UIView!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var quitButton: UIButton!
    
//MARK: - Constants and variables
    var playTime = Manager.shared.playTime
    
//MARK: - Lifecycle functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.65)
        popUpBox.backgroundColor = UIColor.black.withAlphaComponent(0.65)
        self.startAnimation()
        scoreLabel.text = String(Manager.shared.score)
        quitButton.layer.cornerRadius = 5
        quitButton.backgroundColor = .violetColor
        quitButton.applyShadow(color: .violetColor, opacity: 0.7, offset: .zero, radius: 4, viewCornerRadius: 5)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let scores = Scores()
        scores.name = UserDefaults.standard.object(forKey: Keys.nameTextFieldText.rawValue) as? String
        scores.totalScore = scoreLabel.text
        scores.totalTime = String(Int(playTime))
        scores.finishDate = Date()
        Manager.shared.saveScoresArray(item: scores)
    }
    
//MARK: - IBActions
    @IBAction func quitButtonPressed(_ sender: UIButton) {
        Manager.shared.scoreCountStop()
        self.navigationController?.popToRootViewController(animated: true)
        self.removeFromParent()
        self.view.removeFromSuperview()
    }
    
//MARK: - Flow functions
    func startAnimation() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.3) {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }
    }
    
}
